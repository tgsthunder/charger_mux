/*
battery_status.cpp class for monitoring batteries.
Written by Cameron D. Grant
Date: 21/10/2019
*/
#include "battery_status.h"
Battery_Status::Battery_Status(int pin)
{
	hardware_address=pin;
	present=false;
	charged=false;
	last_voltage=0;
}
int Battery_Status::check()
{ //
	int vin=((float)analogRead(hardware_address)*VInCompensation * 550/1023.0);  //10 x input voltage 
	if (vin<BATTERY_PRESENT_MINIMUM_VOLTAGE)
	{ //Voltage is VERY low... therefore battery has been removed.
		present=false;
	} 
	else 
	{ //voltage is high enough to indicate that battery is here
		present=true;
		if (last_voltage<BATTERY_PRESENT_MINIMUM_VOLTAGE)
		{//batttery has just been replaced, so needs charging
			charged=false;
		}
	}
	last_voltage=current_voltage;
	current_voltage=vin;
	return vin;
}
bool Battery_Status::battPresent()
{
	return present;
}
bool Battery_Status::readBattCharged()
{
	return charged;
}
//void Battery_Status::setBattCharged(bool chargeState)
//{	
//	charged=chargeState;
//}
bool Battery_Status::checkCurrent(int current)
{
	if (current<CUTOFF_CURRENT) //we've hit our nominated charge current ... set this battery as charged
  {
    	charged=true;
  }
	return charged;
}

Data::Data(void)
{//initialise Data
  battery=0;
  voltage=0;
  currentx10=0;
  power_relay=OFF;
}
bool Data::Update(void)
{
  //as supply voltage drops, REFF33 (3.3 volts), and all adc reads, will start to read high
  float REF33=(float) analogRead(ADC_3_3_REF)/1023.0*50.0;
  batt[BATT1].VInCompensation=batt[BATT2].VInCompensation=1;//33.0/REF33;

  // Check Battery Voltages
  int vb1=batt[BATT1].check();
  int vb2=batt[BATT2].check();

  // get the current battery voltage
  voltage = battery ? vb2: vb1;

  // get the charging current
  currentx10 = ((float)analogRead(ADC_CURRENT_PIN)*batt[0].VInCompensation - ADC_CURRENT_OFFSET) * ADC_CURRENT_SCALE;

  // if current battery is present and not charged, turn on power relay
  if (batt[battery].battPresent() && !batt[battery].readBattCharged())
  {
    // need to start charging
    if(power_relay == OFF)
    {
      power_relay = ON;
      digitalWrite(POWER_RELAY_PIN, power_relay);  // power relay on
      digitalWrite(SELECT_RELAY_PIN, battery);     // select relevant battery
      delay(10000); // delay 10s to allow charge current to ramp up
    }
    else // charging started previously
    {
      // check if current battery is charged
      batt[battery].checkCurrent(currentx10);
    }
  }

  // if current battery is not present or is charged, swap to other battery for next cycle
  if (!readPresent(battery) || readCharged(battery))
  {
    battery = !battery; // swap to other battery
    power_relay = OFF;
    digitalWrite(POWER_RELAY_PIN, power_relay); // update relay output
    digitalWrite(SELECT_RELAY_PIN, 0);          // turn off select relay
  }

  Serial.print(" PowerRelay=");
  Serial.print(power_relay);
  Serial.print(" BattRelay=");
  Serial.print(battery);
  Serial.print(" REF=");
  Serial.print(REF33);
  Serial.print(" VB1=");
  Serial.print(vb1);
  Serial.print(" VB2=");
  Serial.print(vb2);
  Serial.print(" Battery=");
  Serial.print(battery);
  Serial.print(" Current=");
  Serial.println(currentx10);

  return power_relay;  
}
int Data::readBattery()
{
  return battery;
}
int Data::readVoltage()
{
  return voltage;
}
int Data::readCurrentx10()
{
  return currentx10;
}
bool Data::readPowerRelay()
{
  return power_relay;
}
bool Data::readPresent(int battery)
{
	return batt[battery].battPresent();
}
bool Data::readCharged(int battery)
{
	return batt[battery].readBattCharged();
}
