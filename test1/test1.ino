#define ADC_VOLTAGE_PIN_BATT1    A0
#define ADC_VOLTAGE_PIN_BATT2    A1
#define ADC_CURRENT_PIN    A2
#define ADC_3_3_REF  A3
#define ADC_CURRENT_OFFSET 510.0
#define ADC_CURRENT_SCALE  0.74
#define SELECT_RELAY_PIN   7
#define POWER_RELAY_PIN 6
int currentx10, V1,V2,V33;
void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("TGS Current Test");
  pinMode(ADC_VOLTAGE_PIN_BATT1, INPUT);
  pinMode(ADC_VOLTAGE_PIN_BATT2, INPUT);
  pinMode(ADC_CURRENT_PIN, INPUT);
  pinMode(ADC_3_3_REF, INPUT);
  pinMode(SELECT_RELAY_PIN, OUTPUT);
  pinMode(POWER_RELAY_PIN, OUTPUT);
  digitalWrite(POWER_RELAY_PIN, 1);
  digitalWrite(SELECT_RELAY_PIN, 0);
}

void loop() {
  // put your main code here, to run repeatedly: *batt[0].VInCompensation
  V1=analogRead(ADC_VOLTAGE_PIN_BATT1);
  V2=analogRead(ADC_VOLTAGE_PIN_BATT2);
  V33=analogRead(ADC_3_3_REF);
  currentx10 = ((float)analogRead(ADC_CURRENT_PIN) - ADC_CURRENT_OFFSET) * ADC_CURRENT_SCALE;
  Serial.print("V1= ");
  Serial.print(V1);
  Serial.print(" V2= ");
  Serial.print(V2);
  Serial.print(" V33= ");
  Serial.print(V33);
  Serial.print(" Current=");
  Serial.println(currentx10);
  delay(1000);
}
