#include <TFT.h>
#include <SPI.h>
#include <stdlib.h>
#include "config.h"
#include "battery_status.h"
#include <LowPower.h>
//*******Global variables*******
TFT myScreen = TFT(CS, DC, RESET);
char text_buf[32];
char dispbuff[5];
int colour[3];
Data data; //Data is a class defined in battery_status.h and battery_status.cpp
//*******Declarations*******
void update_measurements(void);
void update_display(void);
void write_display(int line, int side,char* dat);
void write_display_wide(int line, char* dat);
void write_display_colour(int line, int side, char* dat,int* bg);
//*******setup*******
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("TGS Battery Charger Controller");
  pinMode(SELECT_RELAY_PIN, OUTPUT);
  pinMode(POWER_RELAY_PIN, OUTPUT);
  pinMode(ADC_VOLTAGE_PIN_BATT1, INPUT);
  pinMode(ADC_VOLTAGE_PIN_BATT2, INPUT);
  pinMode(ADC_CURRENT_PIN, INPUT);
  pinMode(ADC_3_3_REF, INPUT);
  pinMode(A4, OUTPUT);
  digitalWrite(A4, 0);
  pinMode(A5, OUTPUT);
  digitalWrite(A5, 0);
  myScreen.begin();
  myScreen.background(0,0,0);
  data.Update();
}
//*******loop*******
void loop() {
  // put your main code here, to run repeatedly:
  //data.Update();
  update_measurements();
  update_display();
  delay(1000); //once per second
  //LowPower.idle(SLEEP_10S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, 
  //               SPI_OFF, USART0_OFF, TWI_OFF);
}
//*******upadate_measurements*******
void update_measurements(void)
{
  data.Update();
}
//*******update_display*******
void update_display(void)
{
  // clear section and print voltage
  int voltsx10=data.readVoltage();
  int currentx10=data.readCurrentx10();
  sprintf(text_buf, "v=%d.%d", voltsx10/10,voltsx10%10);
  write_display_wide(0,text_buf);
  sprintf(text_buf, "i=%d.%d", currentx10/10,currentx10%10);
  write_display_wide(1,text_buf);
  
  sprintf(text_buf, "p1=%d", data.readPresent(BATT1));
  write_display(2,0,text_buf);
    sprintf(text_buf, "p2=%d", data.readPresent(BATT2));
  write_display(2,1,text_buf);
  sprintf(text_buf, "B=%d",data.readBattery());
  write_display(3,1,text_buf);
  sprintf(text_buf,"");
  if (data.readPowerRelay())
  { //charging something... indicate this
	 colour[0]=0;
	colour[1]=255;
	colour[2]=0;
  }else
  { //finished charging for now...
	colour[0]=255;
	colour[1]=0;
	colour[2]=0;  
  }
  write_display_colour(4,0,text_buf,colour);
  write_display_colour(4,1,text_buf,colour);
  colour[0]=0;
  colour[1]=0;
  colour[2]=0;
  if(data.readPresent(BATT1))
  { //first assume not charged and not charging.
	  colour[0]=255;
	  colour[1]=0;
	  colour[2]=0;
	  if (data.readBattery()==BATT1) colour[1]=165; //charging
	  if (data.readCharged(BATT1)==true)
	  { //batt is present and charged...
		  colour[0]=0;
		  colour[1]=255;
	  }
  }
  write_display_colour(5,0,text_buf,colour);  //battery 1 status colour indicator
  colour[0]=0;
  colour[1]=0;
  colour[2]=0;
  if(data.readPresent(BATT2))
  { //first assume not charged and not charging.
	  colour[0]=255;
	  colour[1]=0;
	  colour[2]=0;
	  if (data.readBattery()==BATT2) colour[1]=165; //charging
	  if (data.readCharged(BATT2)==true)
	  { //batt is present and charged...
		  colour[0]=0;
		  colour[1]=255;
	  }
  }
  write_display_colour(5,1,text_buf,colour); //battery 2 status colour indicator
}

void write_display_wide(int line, char* dat)
{
	//clear section and print dat
	myScreen.stroke(0,0,0);
	myScreen.fill(0,0,0);
	myScreen.rect(0,10+line*20,128,15);
	myScreen.setTextSize(2);
	myScreen.stroke(255,255,255);  //white
	myScreen.text(dat,2,10+line*20);
}
void write_display(int line, int side,char* dat)
{
	int blackcolour[]={0,0,0};
	write_display_colour(line,side,dat,blackcolour);
}
void write_display_colour(int line, int side, char* dat,int* bg)
{
		strncpy(dispbuff,dat,5);
	//clear section and print dat
	int x=64*side;
	myScreen.stroke(bg[0],bg[1],bg[2]);
	myScreen.fill(bg[0],bg[1],bg[2]);
	myScreen.rect(x,10+line*20,x+64,15);//93 -> 50
	//myScreen.fill(255,0,0);
	//myScreen.rect(x,10+line*20,x+64,15);//93 -> 50
	myScreen.setTextSize(2);
	myScreen.stroke(255,255,255);  //white
	myScreen.text(dispbuff,2+ x , 10+line*20);
}
