//*******Operational defines*******
#define CUTOFF_CURRENT 30
#define BATTERY_PRESENT_MINIMUM_VOLTAGE 200

//*******Pin configuration and calibration defines*******
#define CS    10
#define DC    9
#define RESET 8

#define ADC_VOLTAGE_PIN_BATT1    A0
#define ADC_VOLTAGE_PIN_BATT2    A1
#define ADC_CURRENT_PIN    A2
#define ADC_3_3_REF	A3
#define ADC_CURRENT_OFFSET 510.0
#define ADC_CURRENT_SCALE  0.74  //74mA per step  x 10

#define SELECT_RELAY_PIN   7
#define POWER_RELAY_PIN 6
#define BATT1 0
#define BATT2 1
#define ON  1
#define OFF 0
