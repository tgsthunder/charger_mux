/*
battery_status.h class for monitoring batteries.
Written by Cameron D. Grant
Date: 21/10/2019
*/
#ifndef battery_status_h
#define battery_status_h
#include <Arduino.h>
#include "config.h"
class Battery_Status
{
	public:
		Battery_Status(int pin);
		int check();
		bool battPresent();
		bool readBattCharged();
		//void setBattCharged(bool chargeState);
		float VInCompensation;
		bool checkCurrent(int current);
	private:
		int hardware_address;	//hardware location of this battery voltage (Pin)
		bool present;		//is the battery connected?
		bool charged;		//is this battery charged?
		unsigned int current_voltage;	//voltage just read.
		unsigned int last_voltage;		//last voltage read. <--- do we need this?
};

class Data
{
	public:
		Data(void);
		bool Update(void); //returns charging status for power relay
		int readBattery();
		int readVoltage();
		int readCurrentx10();
		bool readPowerRelay();
		bool readPresent(int battery);
		bool readCharged(int battery);
	private: 
		int battery;
		int voltage;
		int currentx10;
		bool power_relay;
		Battery_Status batt[2]={ADC_VOLTAGE_PIN_BATT1,ADC_VOLTAGE_PIN_BATT2};
};
#endif
